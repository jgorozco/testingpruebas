//
//  ViewController.h
//  acelerometro_ex
//
//  Created by jose garcia orozco on 28/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
@interface ViewController : UIViewController <UIAccelerometerDelegate>

{
    float intx;
    float inty;
    float intz;

}

@property (nonatomic, retain) UIAccelerometer *accelerometer;
@property (nonatomic, retain) CMMotionManager *motionManager;
@property (strong, nonatomic) IBOutlet UIProgressView *acelx;
@property (strong, nonatomic) IBOutlet UIProgressView *acely;
@property (strong, nonatomic) IBOutlet UIProgressView *acelz;

@property (strong, nonatomic) IBOutlet UIProgressView *incx;
@property (strong, nonatomic) IBOutlet UIProgressView *incy;
@property (strong, nonatomic) IBOutlet UIProgressView *incz;
@property (strong, nonatomic) IBOutlet UILabel *lblAcelx;
@property (strong, nonatomic) IBOutlet UILabel *lblAcely;
@property (strong, nonatomic) IBOutlet UILabel *lblAcelz;

@property (strong, nonatomic) IBOutlet UILabel *lblInclinacionx;
@property (strong, nonatomic) IBOutlet UILabel *lblInclinaciony;
@property (strong, nonatomic) IBOutlet UILabel *lblInclinacionz;
@property (strong, nonatomic) IBOutlet UISwitch *isSaking;

@end
