//
//  TwitterSettings.h
//  ejemplo_twitter
//
//  Created by jose garcia orozco on 29/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>

#define kTwitterSettingsButtonIndex 0

@interface TwitterSettings : NSObject

+ (BOOL)hasAccounts;
+ (void)openTwitterAccounts;

@end
