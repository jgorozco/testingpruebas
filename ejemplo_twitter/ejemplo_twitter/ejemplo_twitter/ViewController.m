//
//  ViewController.m
//  ejemplo_twitter
//
//  Created by jose garcia orozco on 29/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"
#import "TwitterSettings.h"
#import <Twitter/Twitter.h>
@interface ViewController ()

@end

@implementation ViewController
@synthesize textView,account;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (IBAction)logintwitter:(id)sender {
    if (![TwitterSettings hasAccounts]) {
        
        [TwitterSettings openTwitterAccounts];
    }else{
        NSLog(@"_____________________");
         ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountTypeTwitter =[accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [accountStore requestAccessToAccountsWithType:accountTypeTwitter
                                         withCompletionHandler:^(BOOL granted, NSError *error) {
                                             if(granted) {
                                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                                     NSLog(@"Account store:%@",[accountStore accountsWithAccountType:accountTypeTwitter]);
                                                     account = [[accountStore
                                                                 accountsWithAccountType:accountTypeTwitter]objectAtIndex:0 ];
                                                     NSLog(@"Account:%@",account.username);
                                                     [self getListFriends];
                                                    
                                                 });
                                             }
                                         }];
    }

}

-(void)getListFriends
{
    TWRequest *postRequest = [[TWRequest alloc]
                              initWithURL:
                              [NSURL URLWithString:@"http://api.twitter.com/1/statuses/friends.json?screen_name=josevallekas"]
                              parameters:nil
                              requestMethod:TWRequestMethodGET];
    [postRequest setAccount:account];
    [postRequest performRequestWithHandler:^(NSData *responseData,
                                             NSHTTPURLResponse *urlResponse,
                                             NSError *error) {
        if ([urlResponse statusCode] == 200) {
            NSError *jsonError = nil;
            NSArray *friendlist = [NSJSONSerialization JSONObjectWithData:responseData
                                                                  options:0
                                                                    error:&jsonError];
            for (NSDictionary *friend in friendlist)
            {
                NSLog(@"Friend:%@",[friend objectForKey:@"name"]);
                
            }
            
        }else{
            NSLog(@"Error:%d",[urlResponse statusCode]);
        }
    }];


}

- (IBAction)postTwitter:(id)sender {
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:textView.text];
//Solo una imagen y un enlace
        [tweetSheet addURL:[NSURL URLWithString:@"http://www.moobapps.com/"]];
        [tweetSheet addImage:[UIImage imageNamed:@"cloud.png"]];

	    [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {/*
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"Configura tu cuenta de twitter"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];*/
      
    }
    
    
}
@end
