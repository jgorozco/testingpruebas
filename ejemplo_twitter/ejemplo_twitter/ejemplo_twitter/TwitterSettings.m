//
//  TwitterSettings.m
//  ejemplo_twitter
//
//  Created by jose garcia orozco on 29/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "TwitterSettings.h"

@implementation TwitterSettings

+ (BOOL)hasAccounts {
    // For clarity
    return [TWTweetComposeViewController canSendTweet];
}

+ (void)openTwitterAccounts {
    
    TWTweetComposeViewController *ctrl = [[TWTweetComposeViewController alloc] init];
    if ([ctrl respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
        // Manually invoke the alert view button handler
        [(id <UIAlertViewDelegate>)ctrl alertView:nil
                             clickedButtonAtIndex:kTwitterSettingsButtonIndex];
    }
}



@end
