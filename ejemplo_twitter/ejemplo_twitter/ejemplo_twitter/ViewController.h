//
//  ViewController.h
//  ejemplo_twitter
//
//  Created by jose garcia orozco on 29/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>

@interface ViewController : UIViewController

- (IBAction)logintwitter:(id)sender;
- (IBAction)postTwitter:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *textView;
@property(strong,nonatomic)        ACAccount *account;
@end
