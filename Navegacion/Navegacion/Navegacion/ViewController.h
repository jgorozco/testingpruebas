//
//  ViewController.h
//  Navegacion
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *boton1;
@property (nonatomic, strong) IBOutlet UIButton *boton2;

@end
