//
//  vista2.m
//  Navegacion
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "vista2.h"
#import "ViewController.h"
#import "vista3.h"

@interface vista2 ()

@end

@implementation vista2
@synthesize boton1;
@synthesize boton2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)volver:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)goto3:(id)sender
{
    vista3 *v3 = [[vista3 alloc] initWithNibName:@"vista3" bundle:nil];
    [self.navigationController pushViewController:v3 animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
