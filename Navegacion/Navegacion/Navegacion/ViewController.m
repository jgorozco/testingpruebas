//
//  ViewController.m
//  Navegacion
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "vista2.h"
#import "vista3.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize boton1;
@synthesize boton2;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(IBAction)goto2:(id)sender
{
    vista2 *v2 = [[vista2 alloc] initWithNibName:@"vista2" bundle:nil];
    [self.navigationController pushViewController:v2 animated:YES];
}

-(IBAction)goto3:(id)sender
{
    vista3 *v3 = [[vista3 alloc] initWithNibName:@"vista3" bundle:nil];
    [self.navigationController pushViewController:v3 animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
