//
//  MasterViewController.h
//  pruebas40
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class DetailViewController;

@interface MasterViewController : UIViewController <NSURLConnectionDelegate,UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSMutableDictionary *dicc;

@property (strong, nonatomic) NSMutableArray *json;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activador;

@property (strong, nonatomic) NSMutableData *receivedData;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
