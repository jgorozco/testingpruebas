//
//  DetailViewController.m
//  pruebas40
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
- (void)configureView:(NSMutableDictionary*) detailItem;
@end

@implementation DetailViewController

@synthesize detailItem;
@synthesize label1;
@synthesize label2;
@synthesize label3;
@synthesize label4;
@synthesize label5;
@synthesize label6;
@synthesize img;

#pragma mark - Managing the detail item

- (void)configureView:(NSMutableDictionary*) detailItem
{   

    NSLog(@"Configureview::: detailitem: %@",self.detailItem);    
    // PARA MOSTRAR DESDE PLIST
    //NSLog(@"Diccionario: %@",self.detailItem);
    //NSString *nombre = [self.detailItem objectForKey:@"nombre"];
    //NSLog(@"Valor de entrada: %@",nombre);
    //NSString *tipo = [self.detailItem objectForKey:@"tipo"];
    //NSLog(@"Valor de entrada: %@", tipo);
    //NSString *descripcion = [self.detailItem objectForKey:@"descripcion"];
    //NSLog(@"Valor de entrada: %@", descripcion);
    //self.label1.text = nombre; 
    //self.label2.text = tipo;
    //self.label3.text = descripcion;
    //Update the user interface for the detail item.
    
    //PARA MOSTRAR DESDE URL
    NSString *description = [self.detailItem objectForKey:@"description"];
    self.label1.text = description;
    NSString *identificador = [self.detailItem objectForKey:@"id"];
    self.label2.text = identificador;
    NSString *precio = [self.detailItem objectForKey:@"price"];
    self.label3.text = precio;
    //id timeStamp = [self.detailItem objectForKey:@"timeStamp"];
    //NSLog(@"timeStamp: %@",timeStamp);
    //self.label4.text = timeStamp;
    NSString *urlData = [self.detailItem objectForKey:@"urlData"];
    self.label5.text = urlData;
    NSString *propietario = [self.detailItem objectForKey:@"userPropietary"];
    self.label6.text = propietario;
    NSString *foto = [self.detailItem objectForKey:@"urlPhoto"];
    NSURL *url = [NSURL URLWithString:foto];
    NSData *urlFoto = [NSData dataWithContentsOfURL:url];
    UIImage *imagen = [UIImage imageWithData:urlFoto];
    img.image = imagen;
}

- (void)viewDidLoad
{	
    NSLog(@"[2]DetailViewController::viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView:detailItem];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.label1 = nil;
    self.label2 = nil;
    self.label3 = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self configureView:detailItem];
    NSLog(@"[2]DetailViewController::: viewdidAppear");
    NSLog(@"[2]Diccionario: %@",self.detailItem);
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"[2]DetailViewController::: viewWillAppear");
    NSLog(@"[2]Diccionario: %@",self.detailItem);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
    }
    return self;
}
							
@end
