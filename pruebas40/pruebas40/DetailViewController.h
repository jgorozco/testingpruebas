//
//  DetailViewController.h
//  pruebas40
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <NSURLConnectionDelegate>

@property (strong, nonatomic) NSMutableDictionary *detailItem;

@property (strong, nonatomic) IBOutlet UILabel * label1;
@property (strong, nonatomic) IBOutlet UILabel * label2;
@property (strong, nonatomic) IBOutlet UILabel * label3;
@property (strong, nonatomic) IBOutlet UILabel * label4;
@property (strong, nonatomic) IBOutlet UILabel * label5;
@property (strong, nonatomic) IBOutlet UILabel * label6;

@property (strong, nonatomic) IBOutlet UIImageView * img;

@end
