//
//  MasterViewController.m
//  pruebas40
//
//  Created by Kike on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Foto.h"



@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

@synthesize json;
@synthesize tableView;
@synthesize detailViewController = _detailViewController;
@synthesize dicc;
@synthesize activador;
@synthesize receivedData;
@synthesize fetchedResultsController;
@synthesize managedObjectContext = _managedObjectContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Master", @"Master");
    }
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [activador startAnimating];
    [receivedData setLength:json.count];
    NSLog(@"Respuesta recibida");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"MasterViewController::: didFailWithError");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error de conexión" message:@"No se ha podido establecer conexión con el servidor."
        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [activador stopAnimating];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    NSLog(@"Recogiendo datos..");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.json = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:nil];
    [activador stopAnimating];
    NSLog(@"Conexion finalizada, parando rueda");
    [self.tableView reloadData];
}

- (void)viewDidLoad
{   
    NSURLRequest *dir = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://therightpriceapp.appspot.com/getBids"]cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:dir delegate:self];
    [activador startAnimating];
    [theConnection start];
    NSLog(@"Inicio de conexión");
    if (theConnection == nil) {
        NSLog(@"Conexión fallida...");
    }
    else {
        receivedData = [NSMutableData data];
    }
    
    // PARA INICIALIZAR CON NOMBRES DE POKEMON Y QUE MUESTRE EL NOMBRE
    // _objects = [NSMutableArray arrayWithObjects:@"Pikachu",@"Bulbasaur",@"Charmander",@"Squirtle",@"Ditto",@"Snorlax",@"Articuno",@"Moltres",@"Zapdos",@"Mewtwo",@"Piedra", nil];
    
    
    
    // PARA INICIALIZAR DESDE UN DICCIONARIO ALOJADO EN UN PLIST QUE HE CREADO EN RESOURCES
    //NSString *pathDiccionario = [[NSBundle mainBundle] pathForResource:@"Pokemon" ofType:@"plist"];
    //dicc = [[NSMutableDictionary alloc] initWithContentsOfFile:pathDiccionario];
    //NSString *nombre = [self.dicc objectForKey:@"nombre"];
    //_objects = [NSMutableArray arrayWithObject:nombre];
    
    
    // PARA INICIALIZAR DESDE UN JSON (TO A DICTIONARY)
    //NSString *pathJSON = [[NSBundle mainBundle] pathForResource:@"JSONPokemon" ofType:@"json"];
    //NSData *datosJSON = [[NSData alloc] initWithContentsOfFile:pathJSON];
    //self.json = [NSJSONSerialization JSONObjectWithData:datosJSON options:0 error:nil];
    //NSLog(@"Datos JSON: %@",json);
    
    // PARA INICIALIZAR DESDE URL COGIENDO UN JSON
    NSURL *url = [NSURL URLWithString:@"http://therightpriceapp.appspot.com/getBids"];
    NSData *jsonreturn = [[NSData alloc] initWithContentsOfURL:url];
    self.json = [NSJSONSerialization JSONObjectWithData:jsonreturn options:0 error:nil];
    NSLog(@"jsonreturn = %@",json);
    
    int size = json.count;
    
    NSLog(@"Petazo en breves...");
    for (int i=0; i < size; i++) {
        NSDictionary *object = [json objectAtIndex:i];
        NSLog(@"Cogemos una entidad foto para rellenarla");
        // FALLA AL COGER LA ENTIDAD PARA TRATARLA: DICE QUE NO EXISTE.
        Foto *foto = [NSEntityDescription insertNewObjectForEntityForName:@"Foto" inManagedObjectContext:_managedObjectContext];
        
        NSLog(@"Añadimos los datos a la entidad");
        foto.descripcion = [object objectForKey:@"descrition"];
        foto.price = [object objectForKey:@"price"];
        foto.urlfoto = [object objectForKey:@"urlPhoto"];
        
        NSLog(@"Salvamos el contexto");
        [_managedObjectContext save:nil];
    }
    
    NSLog(@"Petazo sobrepasado");    
    
    NSLog(@"MasterViewController:: viewDidLoad");
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Foto" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSError *error = nil;
    NSMutableArray *resultadoConsulta = [[_managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (resultadoConsulta == nil) {
        NSLog(@"Error en Core Data");
    }
    NSLog(@"RESULTADO: %@",resultadoConsulta);
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    NSLog(@"MAsterViewController:: viewDidUnload");
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"MasterViewController::: viewdidAppear");
}

-(void)viewWillAppear:(BOOL)animated
{
   NSLog(@"MasterViewController::: viewWillAppear");
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"MasterViewController::: viewDidDisappear");
}

-(void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"MasterViewController::: viewWillDisappear");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

/* - (void)setupFetchedResultsController
{
    // 1 - Decide what Entity you want
    NSString *entityName = @"Foto"; // Put your entity name here
    NSLog(@"Setting up a Fetched Results Controller for the Entity named %@", entityName);
    
    // 2 - Request that Entity
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    
    // 3 - Filter it if you want
    //request.predicate = [NSPredicate predicateWithFormat:@"Role.name = Blah"];
    
    // 4 - Sort it if you want
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                                     ascending:YES
                                                                                      selector:@selector(localizedCaseInsensitiveCompare:)]];
    // 5 - Fetch it
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}*/

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return json.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
   /* Foto *foto = [NSEntityDescription insertNewObjectForEntityForName:@"Foto" inManagedObjectContext:self.managedObjectContext];
    */
    NSDictionary *object = [json objectAtIndex:indexPath.row];
    /*
    foto.descripcion = [object objectForKey:@"descrition"];
    foto.price = [object objectForKey:@"price"];
    foto.urlfoto = [object objectForKey:@"urlPhoto"];
    
    [self.managedObjectContext save:nil];
    
    
    */
    NSString *nombre = [object objectForKey:@"id"];
    NSString *photo = [object objectForKey:@"urlPhoto"];
    NSURL *url = [NSURL URLWithString:photo];
    NSData *urlFoto = [NSData dataWithContentsOfURL:url];
    UIImage *imagen = [UIImage imageWithData:urlFoto];
    cell.textLabel.text = nombre;
    cell.image = imagen;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Cambiando a los detalles");
    
    if (!self.detailViewController) {
        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    }
    
    //EN LOCAL
    
    //int x = indexPath.row;
    //NSLog(@"Valor del indexpath.row: %d",x);
    //NSMutableDictionary *d = [json objectAtIndex:x];
    //self.detailViewController.detailItem = d;
    //[self.navigationController pushViewController:self.detailViewController animated:YES];
    
    // CON URL
    
    int x = indexPath.row;
    NSMutableDictionary *d = [json objectAtIndex:x];
    self.detailViewController.detailItem = d;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

@end
