//
//  Foto.h
//  pruebas40
//
//  Created by pcoellov on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Foto : NSManagedObject

@property (nonatomic, retain) NSString * descripcion;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * urlfoto;

@end
