//
//  coreDataViewController.m
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "coreDataViewController.h"
#import "AppDelegate.h"
#import "coreDataViewcontrollerBuscar.h"

@interface coreDataViewController ()

@end

@implementation coreDataViewController

@synthesize nombre,tipo,evolucion,estado;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/**
 
    Método que utilizamos para ocultar el teclado al dar al botón return. 
    Tenemos que asociar el campo de texto con la función "did end on exit" con esta.
 
 */

-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder]; 
}

/**
 
    Método que usamos para guardar datos en el Coredata: 
    - Obtenemos el contexto
    - Creamos un objeto nuevo con la entidad que queramos y le damos valores
    - Guardamos el contexto
 
 */

-(void) guardarDatos 
{   
    BOOL flag = TRUE;
    if ((nombre.text.length == 0) | (tipo.text.length == 0) | (evolucion.text.length == 0))
        flag = FALSE;
    if (flag) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *nuevoPokemon;
        nuevoPokemon = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Pokemon"
                  inManagedObjectContext:context];
        [nuevoPokemon setValue:nombre.text forKey:@"nombre"];
        [nuevoPokemon setValue:tipo.text forKey:@"tipo"];
        [nuevoPokemon setValue:evolucion.text forKey:@"evolucion"];
        NSLog(@"Contacto creado con datos: %@, %@, %@",nombre.text,tipo.text,evolucion.text);
        nombre.text = @"";
        tipo.text = @"";
        evolucion.text = @"";
        NSError *error;
        [context save:&error];
        estado.text = @"Pokemon almacenado";
    }
    else {
        estado.text = @"Debe rellenar todos los datos";
    }
}


@end
