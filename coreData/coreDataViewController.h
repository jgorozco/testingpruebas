//
//  coreDataViewController.h
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface coreDataViewController : UIViewController <UITextFieldDelegate> {
    UITextField *nombre;
    UITextField *tipo;
    UITextField *evolucion;
    UILabel *estado;
}

@property (strong, nonatomic) IBOutlet UITextField *nombre;
@property (strong, nonatomic) IBOutlet UITextField *evolucion;
@property (strong, nonatomic) IBOutlet UITextField *tipo;
@property (strong, nonatomic) IBOutlet UILabel *estado;

- (IBAction) guardarDatos;

@end
