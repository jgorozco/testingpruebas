//
//  coreDataViewControllerBuscar.m
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "coreDataViewControllerBuscar.h"
#import "AppDelegate.h"

@interface coreDataViewControllerBuscar () {
    
}

@end

@implementation coreDataViewControllerBuscar

@synthesize nombreB, tipoB, evolucionB, estadoB;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/**
 
    Método que utilizamos para ocultar el teclado al dar al botón return. 
    Tenemos que asociar el campo de texto con la función "did end on exit" con esta.
 
 */

-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder]; 
}

/**
 
    Método que usamos para buscar datos en el Coredata: 
    - Obtenemos el contexto
    - Obtenemos la entidad que queramos
    - Creamos una petición (añadiendole un predicado)
 [fetchRequest: SELECT * FROM 'ENTITY' + NSpredicate: WHERE 'sentencia']
    - Ejecutamos la petición y guardamos todos los resultados
    - Mostramos por pantalla el primero de los resultados
 
 */

-(void) encontrarPokemon
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(nombre = %@)", nombreB.text];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"ARRAY DE OBJETOS: %@",objects);
    if ([objects count] == 0) {
        estadoB.text = @"Sin resultado";
    } else {
        matches = [objects objectAtIndex:0];
        tipoB.text = [matches valueForKey:@"tipo"];
        evolucionB.text = [matches valueForKey:@"evolucion"];
        estadoB.text = [NSString stringWithFormat:
                       @"%d resultado/s encontrado/s", [objects count]];
    }
}

@end
