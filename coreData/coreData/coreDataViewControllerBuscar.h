//
//  coreDataViewControllerBuscar.h
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface coreDataViewControllerBuscar : UIViewController {
    UITextField *nombreB;
    UILabel *tipoB;
    UILabel *evolucionB;
    UILabel *estadoB;
}

@property (strong, nonatomic) IBOutlet UITextField *nombreB;
@property (strong, nonatomic) IBOutlet UILabel *evolucionB;
@property (strong, nonatomic) IBOutlet UILabel *tipoB;
@property (strong, nonatomic) IBOutlet UILabel *estadoB;

- (IBAction) encontrarPokemon;

@end