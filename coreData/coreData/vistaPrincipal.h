//
//  vistaPrincipal.h
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface vistaPrincipal : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *nuevo;
@property (strong, nonatomic) IBOutlet UIButton *buscar;
@property (strong, nonatomic) IBOutlet UIButton *lista;

@end
