//
//  pokemonDetalle.h
//  coreData
//
//  Created by pcoellov on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pokemonDetalle : UIViewController

@property (strong, nonatomic) NSMutableDictionary *detailItem;

@property (strong, nonatomic) IBOutlet UILabel *nombre;
@property (strong, nonatomic) IBOutlet UILabel *evolucion;
@property (strong, nonatomic) IBOutlet UILabel *tipo;

@end
