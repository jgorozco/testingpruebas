//
//  pokemonDetalle.m
//  coreData
//
//  Created by pcoellov on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "pokemonDetalle.h"

@interface pokemonDetalle ()
- (void)configureView:(NSMutableDictionary*) detailItem;

@end

@implementation pokemonDetalle

@synthesize nombre; 
@synthesize tipo;
@synthesize evolucion; 
@synthesize detailItem; 

#pragma mark - Managing the detail item

/**
 
    Método que usamos para customizar la vista según los datos que nos lleguen (según 
    la fila de la tabla que hayamos pulsado en la vista anterior
    @parameter detailItem: contiene los detalles de la fila que hemos pulsado y los muestra.
 
 */

-(void)configureView: (NSMutableDictionary *) detailItem{
    NSLog(@"Valor del DetailItem: %@",self.detailItem);
    NSString *n = [self.detailItem valueForKey:@"nombre"];
    NSString *e = [self.detailItem valueForKey:@"evolucion"];
    NSString *t = [self.detailItem valueForKey:@"tipo"];
    // Do any additional setup after loading the view from its nib.
    self.nombre.text = n;
    self.evolucion.text = e;
    self.tipo.text = t;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Valor de detail en detalle: %@",self.detailItem);
    [self configureView:self.detailItem];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
