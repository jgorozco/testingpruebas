//
//  listaPokemon.m
//  coreData
//
//  Created by pcoellov on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "listaPokemon.h"
#import "AppDelegate.h"
#import "pokemonDetalle.h"

@interface listaPokemon ()

@end

@implementation listaPokemon

@synthesize elementos;
@synthesize pokemons;
@synthesize pokemonDetalle;
@synthesize resultadosBusqueda;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 
    Método por defecto de la aplicación, a la que hemos añadido un fetch para que obtenga
 todos los elementos del coreData y poder mostrarlos en una tabla identificada por el nombre.
 Se almacena tanto la lista de elementos como el tamaño de dicha lista.
 
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    self.elementos = [objects count];
    self.pokemons = objects;
}

/**
 
    Método que se llama en cuanto se incluye una letra en el buscador del navigationBar, y que
    continuamente se realimenta con los resultados que va obteniendo. Si el campo de búsqueda 
    esta vacio se muestra la lista completa, sino la lista filtrada acorde a la búsqueda. 
    @parameter controller : controlador de la propia busqueda
    @parameter searchString : Búsquedas que se van pasando continuamente (letra a letra)
 
 */


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString 
{
    NSLog(@"BUSQUEDA SIMILAR A %@",searchString);
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(nombre CONTAINS[cd] %@)", searchString];
    [request setPredicate:pred];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"ARRAY DE OBJETOS: %@",objects);
    resultadosBusqueda = objects;
    [self.tableView reloadData];
    return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Devuelve el número de secciones (1 sección para nuestra tabla)
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Devuelve el número de filas en la sección
    int r = resultadosBusqueda.count;
    if (r == 0)
        return elementos;
    else 
        return resultadosBusqueda.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configuramos las celdas obteniendo el nombre de los elementos de la lista
    NSManagedObject *matches = nil;
    int r = resultadosBusqueda.count;
    if (r == 0) {
        matches = [self.pokemons objectAtIndex:indexPath.row];
    }
    else {
        matches = [self.resultadosBusqueda objectAtIndex:indexPath.row];        
    }
    NSLog(@"Lista general: %@", matches);
    
    NSLog(@"Pokemon encontrado: %@ ",matches);
    NSString *name = [matches valueForKey:@"nombre"];
    cell.textLabel.text = name;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

/**
 
    Método que se ejecuta al seleccionar una de las celdas de la tabla. Para cada elemento que se 
    pulsa se mandan los datos a otra vista que los tratará debidamente. 
 
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Cambiando a los detalles");
    
    self.pokemonDetalle = [[pokemonDetalle alloc] initWithNibName:@"pokemonDetalle" bundle:nil];
    int x = indexPath.row;
    NSLog(@"Valor del indexpath.row: %d",x);
    NSMutableDictionary *d = [self.pokemons objectAtIndex:x];
    NSLog(@"VALOR DE D: %@",d);
    self.pokemonDetalle.detailItem = d;
    NSLog(@"Valor de detailItem en lista: %@",self.pokemonDetalle.detailItem);
    [self.navigationController pushViewController:self.pokemonDetalle animated:YES];

}

@end
