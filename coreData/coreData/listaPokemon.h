//
//  listaPokemon.h
//  coreData
//
//  Created by pcoellov on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class pokemonDetalle;

@interface listaPokemon : UITableViewController <UISearchDisplayDelegate,UISearchBarDelegate>{
}

@property (nonatomic) int elementos;
@property (strong, nonatomic) NSArray *pokemons;
@property (strong, nonatomic) pokemonDetalle *pokemonDetalle;
@property (strong, nonatomic) NSArray *resultadosBusqueda;

@end
