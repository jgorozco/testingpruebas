//
//  vistaPrincipal.m
//  coreData
//
//  Created by Kike on 24/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "vistaPrincipal.h"
#import "coreDataViewController.h"
#import "coreDataViewControllerBuscar.h"
#import "listaPokemon.h"

@interface vistaPrincipal () {
}

@end

@implementation vistaPrincipal

@synthesize nuevo;
@synthesize buscar;
@synthesize lista;


/**
 
    Métodos de navegación entre las vistas.
 
 */

- (IBAction)gotoNuevo:(id)sender {
    NSLog(@"Entrando en nueva vista: INSERTAR POKEMON");
    coreDataViewController *core1 = [[coreDataViewController alloc] initWithNibName:@"coreDataViewController" bundle:nil];
    NSLog(@"VISTA: %@",core1);
    [self.navigationController pushViewController:core1 animated:YES];
}

- (IBAction)gotoBuscar:(id)sender {
    NSLog(@"Entrando en nueva vista: BUSCAR POKEMON");
    coreDataViewControllerBuscar *core2 = [[coreDataViewControllerBuscar alloc] initWithNibName:@"coreDataViewControllerBuscar" bundle:nil];
    [self.navigationController pushViewController:core2 animated:YES];
}

- (IBAction)gotoLista:(id)sender {
    NSLog(@"Entrando en nueva vista: LISTA POKEMON");
    listaPokemon *lp = [[listaPokemon alloc] initWithNibName:@"listaPokemon" bundle:nil];
    [self.navigationController pushViewController:lp animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
