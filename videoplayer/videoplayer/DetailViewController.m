//
//  DetailViewController.m
//  videoplayer
//
//  Created by jose garcia on 29/05/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import "DetailViewController.h"
#define URL @"http://www.ebookfrenzy.com/ios_book/movie/movie.mov"
@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize detailItem = _detailItem;
@synthesize vistavideo = _vistavideo;
@synthesize detailDescriptionLabel = _detailDescriptionLabel;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize moviePlayerController;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *botonprueba = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    botonprueba.frame = CGRectMake(20, 20, 200, 44); 
    [botonprueba setTitle:@"Play in other window" forState:UIControlStateNormal];
    [botonprueba addTarget:self action:@selector(clickOtro:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:botonprueba];
    [self configureView];
}

- (void)viewDidUnload
{
    [self setVistavideo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.detailDescriptionLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
    }
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (IBAction)clickNormal:(id)sender {
    NSString *url=URL;
    NSURL    *fileURL    =   [NSURL URLWithString:url];  
    moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];  
    [moviePlayerController prepareToPlay];
    [moviePlayerController.view setFrame:self.vistavideo.frame]; 
    [self.view addSubview:moviePlayerController.view];  
    moviePlayerController.fullscreen = NO;  
    moviePlayerController.controlStyle = MPMovieControlStyleDefault;
    moviePlayerController.shouldAutoplay = YES;
//    [moviePlayerController setFullscreen:YES animated:YES];
    [moviePlayerController play];  
    
    
}

- (IBAction)clickOtro:(id)sender {
    NSString *url=URL;
    NSURL    *fileURL    =   [NSURL URLWithString:url];  
    MPMoviePlayerViewController *vc=[[MPMoviePlayerViewController alloc]initWithContentURL:fileURL];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
@end
