//
//  DetailViewController.h
//  videoplayer
//
//  Created by jose garcia on 29/05/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h> 

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>
{
    MPMoviePlayerController *moviePlayerController;
}
@property(nonatomic, strong) MPMoviePlayerController *moviePlayerController;
@property (strong, nonatomic) id detailItem;
- (IBAction)clickNormal:(id)sender;
- (IBAction)clickOtro:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vistavideo;

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
