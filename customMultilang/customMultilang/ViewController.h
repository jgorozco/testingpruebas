//
//  ViewController.h
//  customMultilang
//
//  Created by jose garcia orozco on 03/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multilangManager.h"
@interface ViewController : UIViewController
{
    multilangManager *mla;

}
@property (strong, nonatomic) IBOutlet UILabel *textoACargar;
@property (strong, nonatomic) IBOutlet UILabel *idiomaCargado;
- (IBAction)espaniol:(id)sender;
- (IBAction)ingles:(id)sender;

@end
