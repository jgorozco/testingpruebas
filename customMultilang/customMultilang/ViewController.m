//
//  ViewController.m
//  customMultilang
//
//  Created by jose garcia orozco on 03/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize textoACargar;
@synthesize idiomaCargado;

- (void)viewDidLoad
{
    [super viewDidLoad];
    mla=[multilangManager getInstance];
    [self reloadText];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setTextoACargar:nil];
    [self setIdiomaCargado:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)espaniol:(id)sender {
    //CAMBIAR IDIOMA
    [mla setCurrentLanguage:@"es"];
    //CARGAR IDIOMA EN LOS ELEMENTOS DE AL APP
    [self reloadText];

}

- (IBAction)ingles:(id)sender {
    [mla setCurrentLanguage:@"en"];
    [self reloadText];

}

-(void)reloadText
{
    textoACargar.text=[mla getMultilangStr:@"example_1"];
    idiomaCargado.text=[mla getLanguage];


}

@end
