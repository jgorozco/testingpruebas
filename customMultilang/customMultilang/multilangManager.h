//
//  multilangManager.h
//  customMultilang
//
//  Created by jose garcia orozco on 03/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface multilangManager : NSObject
{
    NSString *currentLang;
    NSBundle *bundle;

}
+ (multilangManager *)getInstance;



- (NSString*) getLanguage;
-(NSString *)getCurrentLang;
-(void)setCurrentLanguage:(NSString *)lang;
-(NSString *)getMultilangStr:(NSString *)key;

@end
