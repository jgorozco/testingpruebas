//
//  multilangManager.m
//  customMultilang
//
//  Created by jose garcia orozco on 03/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "multilangManager.h"

@implementation multilangManager

static multilangManager *singleFrwk = nil;

#pragma mark - SINGLETON THINGS

+ (multilangManager *)getInstance
{
    @synchronized(self)
    {
        if (!singleFrwk)
            singleFrwk = [[multilangManager alloc] init];
        
        return singleFrwk;
    }
}




// LocalizationSetLanguage(@"es");
// LocalizationSetLanguage(@"en");
// LocalizationSetLanguage(@"pt");
// LocalizationSetLanguage(@"fr");

- (multilangManager *)init
{
    currentLang=@"es";
    bundle = [NSBundle mainBundle];
    return self;
}

- (NSString*) getLanguage{
    return currentLang;
}

-(NSString *)getCurrentLang
{
    return currentLang;
}

- (void) resetLocalization
{
    NSLog(@"Lang not exist");
    bundle = [NSBundle mainBundle];
}

-(void)setCurrentLanguage:(NSString *)lang
{
    currentLang=lang;
    NSString *path = [[ NSBundle mainBundle ] pathForResource:lang ofType:@"lproj" ];
    if (path == nil)
        //Si no existe el idimoa, pone el por defecto
        [self resetLocalization];
    else
        bundle = [NSBundle bundleWithPath:path];
}

-(NSString *)getMultilangStr:(NSString *)key
{
    return [[bundle localizedInfoDictionary] objectForKey:key];
}


@end
