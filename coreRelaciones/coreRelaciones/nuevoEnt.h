//
//  nuevoEnt.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nuevoEnt : UIViewController {
    UITextField *nombre;
    UITextField *origen;
    UITextField *victorias;
    UILabel *estado;
}

@property (strong, nonatomic) IBOutlet UITextField *nombre;
@property (strong, nonatomic) IBOutlet UITextField *origen;
@property (strong, nonatomic) IBOutlet UITextField *victorias;
@property (strong, nonatomic) IBOutlet UILabel *estado;

-(IBAction)guardarDatos;

@end
