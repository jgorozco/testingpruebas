//
//  nuevoEnt.m
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "nuevoEnt.h"
#import "AppDelegate.h"

@interface nuevoEnt ()

@end

@implementation nuevoEnt

@synthesize nombre, origen, victorias, estado;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void) guardarDatos 
{   
    BOOL flag = TRUE;
    if ((nombre.text.length == 0) | (origen.text.length == 0) | (victorias.text.length == 0))
        flag = FALSE;
    if (flag) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *nuevoPokemon;
        nuevoPokemon = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Entrenador"
                        inManagedObjectContext:context];
        [nuevoPokemon setValue:nombre.text forKey:@"nombre"];
        [nuevoPokemon setValue:origen.text forKey:@"origen"];
        [nuevoPokemon setValue:victorias.text forKey:@"victorias"];
        NSLog(@"Contacto creado con datos: %@, %@, %@",nombre.text,origen.text,victorias.text);
        nombre.text = @"";
        origen.text = @"";
        victorias.text = @"";
        NSError *error;
        [context save:&error];
        estado.text = @"Entrenador almacenado";
    }
    else {
        estado.text = @"Debe rellenar todos los datos";
    }
}

@end
