//
//  vistaPrincipal.m
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "vistaPrincipal.h"
#import "AppDelegate.h"
#import "nuevoPok.h"
#import "nuevoEnt.h"
#import "listaEnt.h"
#import "listaPok.h"

@interface vistaPrincipal ()

@end

@implementation vistaPrincipal

@synthesize insertarPokemon,insertarEntrenador,listaPokemon,listaEntrenador,modificarPokemon;
@synthesize labelEncontrado1, boton1Query,labelEncontrado2,boton2Query,labelEncontrado3,boton3Query;
@synthesize botonObject, labelID,prueba;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)nuevoPokemon:(id)sender 
{
    NSLog(@"Entrando en nueva vista: INSERTAR POKEMON");
    nuevoPok *pok1 = [[nuevoPok alloc] initWithNibName:@"nuevoPok" bundle:nil];
    NSLog(@"VISTA: %@",pok1);
    [self.navigationController pushViewController:pok1 animated:YES];
}

- (IBAction)nuevoEntrenador:(id)sender 
{
    NSLog(@"Entrando en nueva vista: INSERTAR ENTRENADOR");
    nuevoEnt *ent1 = [[nuevoEnt alloc] initWithNibName:@"nuevoEnt" bundle:nil];
    NSLog(@"VISTA: %@",ent1);
    [self.navigationController pushViewController:ent1 animated:YES];
}

- (IBAction)todosPokemon:(id)sender 
{
    NSLog(@"Entrando en nueva vista: LISTA DE POKEMONS");
    listaPok *lpok1 = [[listaPok alloc] initWithNibName:@"listaPok" bundle:nil];
    NSLog(@"VISTA: %@",lpok1);
    [self.navigationController pushViewController:lpok1 animated:YES];
}

- (IBAction)todosEntrenadores:(id)sender 
{
    NSLog(@"Entrando en nueva vista: LISTA DE ENTRENADORES");
    listaEnt *lent1 = [[listaEnt alloc] initWithNibName:@"listaEnt" bundle:nil];
    NSLog(@"VISTA: %@",lent1);
    [self.navigationController pushViewController:lent1 animated:YES];
}

- (IBAction)modificarPokemon:(id)sender
{
    NSLog(@"Modificando todos los pokemon añadiendo un numero al final del nombre");
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    for (int i=0 ; i<objects.count ; i++) {
        NSManagedObject *pok = [objects objectAtIndex:i];
        NSString *nivel = [pok valueForKey:@"nivel"];
        int level = [nivel intValue];
        level = level + 1;
        NSString *levelMod = [NSString stringWithFormat:@"%d",level];
        [pok setValue:levelMod forKey:@"nivel"];
    }
}

- (IBAction)buscarCon1Query:(id)sender 
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSString *numero = [NSString stringWithFormat:@"%d",24];
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"nivel = %@",numero];
    [request setPredicate:pred1];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    int elementos = [objects count];
    self.labelEncontrado1.text = [NSString stringWithFormat:@"[24]%d pokemons encontrados",elementos];
}

- (IBAction)buscarCon2Querys:(id)sender 
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    /*NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"nivel = 24"];
    NSString *rata = [NSString stringWithString:@"Rata"];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"tipo = %@",rata]];
    NSArray *queryfinal = [[NSArray alloc] initWithObjects:pred1,pred2, nil];
    NSPredicate *preds = [NSCompoundPredicate andPredicateWithSubpredicates:queryfinal];
    NSLog(@"Query final::: %@",preds);
    [request setPredicate:preds];*/
    NSString *numero = [NSString stringWithFormat:@"%d",24];
    NSStream *param = [NSString stringWithString:@"Rata"];
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"(nivel contains[cd] %@)",numero];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"(tipo contains[cd] %@)",param];
    NSArray *arrayPreds = [NSArray arrayWithObjects:pred1,pred2, nil];
    NSPredicate *finalPred = [NSCompoundPredicate andPredicateWithSubpredicates:arrayPreds];
    NSLog(@"predicado final??::: %@",finalPred);
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"objects:::: %@",objects);
    NSArray *filtro1 = [objects filteredArrayUsingPredicate:pred1];
    NSLog(@"filtro1:::: %@",filtro1);
    NSArray *filtro2 = [filtro1 filteredArrayUsingPredicate:pred2];
    NSLog(@"filtro2:::: %@",filtro2);
    int elementos = [filtro2 count];
    self.labelEncontrado2.text = [NSString stringWithFormat:@"[Rata/24] %d pokemons encontrados",elementos];
}

- (IBAction)buscarCon3Querys:(id)sender 
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Pokemon" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    /*NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"nivel = 24"];
     NSString *rata = [NSString stringWithString:@"Rata"];
     NSPredicate *pred2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"tipo = %@",rata]];
     NSArray *queryfinal = [[NSArray alloc] initWithObjects:pred1,pred2, nil];
     NSPredicate *preds = [NSCompoundPredicate andPredicateWithSubpredicates:queryfinal];
     NSLog(@"Query final::: %@",preds);
     [request setPredicate:preds];*/
    NSString *nombre = [NSString stringWithString:@"Pikachu"];
    NSString *numero = [NSString stringWithFormat:@"%d",24];
    NSString *param = [NSString stringWithString:@"Rata"];
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"(nivel contains[cd] %@)",numero];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"(tipo contains[cd] %@)",param];
    NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"(nombre contains[cd] %@)",nombre];
    NSArray *arrayPreds = [NSArray arrayWithObjects:pred1,pred2, nil];
    NSPredicate *finalPred = [NSCompoundPredicate andPredicateWithSubpredicates:arrayPreds];
    NSLog(@"predicado final??::: %@",finalPred);
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSArray *filtro1 = [objects filteredArrayUsingPredicate:pred1];
    NSArray *filtro2 = [filtro1 filteredArrayUsingPredicate:pred2];
    NSArray *filtro3 = [filtro2 filteredArrayUsingPredicate:pred3];
    NSLog(@"lista final:::: %@",filtro3);
    int elementos = [filtro3 count];
    self.labelEncontrado3.text = [NSString stringWithFormat:@"[Pikachu/Rata/24] %d pokemons encontrados",elementos];
}

- (IBAction)buscarPorID 
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Entrenador" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *cod = [NSPredicate predicateWithFormat:@"self != %@",[NSString stringWithString:@"E2D2408B-815A-408C-814B-F4BA12EFCB9F/Entrenador/p1"]];
    [request setPredicate:cod];
    NSError *error;
    NSArray *resultado = [context executeFetchRequest:request error:&error];
    if (resultado == nil) {
        self.labelID.text = @"No se ha encontrado";
    }
    else {
        self.labelID.text = [NSString stringWithFormat:@"Se ha encontrado un resultado"];
    }
    NSLog(@"resultado: %@",resultado);
    NSLog(@"valores: %@ / %@ / %@ ", [resultado valueForKey:@"nombre"],[resultado valueForKey:@"origen"],[resultado valueForKey:@"victorias"]);
}

@end
