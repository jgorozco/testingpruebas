//
//  cazadosEnt.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cazadosEnt : UITableViewController

@property (strong, nonatomic) NSMutableDictionary *detailItem;
@property (nonatomic) int elementos;
@property (strong, nonatomic) NSArray *pokemons;

@end
