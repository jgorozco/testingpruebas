//
//  pokemonDetalle.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class entrenadorDetalle;

@interface pokemonDetalle : UIViewController

@property (strong, nonatomic) entrenadorDetalle *entrenadorDetalle;

@property (strong, nonatomic) NSMutableDictionary *detailItem;

@property (strong, nonatomic) IBOutlet UILabel *labelNombre;
@property (strong, nonatomic) IBOutlet UILabel *labelTipo;
@property (strong, nonatomic) IBOutlet UILabel *labelNivel;

-(IBAction)infoTrainer;

@end
