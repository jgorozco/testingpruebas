//
//  DetallesPokemon.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetallesPokemon : UIViewController

@property (strong, nonatomic) NSMutableDictionary *detailItem;

@end
