//
//  pokemonDetalle.m
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "pokemonDetalle.h"
#import "entrenadorDetalle.h"

@interface pokemonDetalle ()

@end

@implementation pokemonDetalle

@synthesize detailItem, labelTipo, labelNivel, labelNombre, entrenadorDetalle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)configureView: (NSMutableDictionary *) detailItem{
    NSLog(@"Valor del DetailItem: %@",self.detailItem);
    NSString *no = [self.detailItem valueForKey:@"nombre"];
    NSString *ti = [self.detailItem valueForKey:@"tipo"];
    NSString *ni = [self.detailItem valueForKey:@"nivel"];
    // Do any additional setup after loading the view from its nib.
    self.labelNombre.text = no;
    self.labelTipo.text = ti;
    self.labelNivel.text = ni;
}

-(void) infoTrainer {
    self.entrenadorDetalle = [[entrenadorDetalle alloc] initWithNibName:@"entrenadorDetalle" bundle:nil];
    NSMutableDictionary *e = [self.detailItem valueForKey:@"trainer"];
    self.entrenadorDetalle.detailTrainer = e;
    [self.navigationController pushViewController:self.entrenadorDetalle animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Valor de detail en detalle: %@",self.detailItem);
    [self configureView:self.detailItem];}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
