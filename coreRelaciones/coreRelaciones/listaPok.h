//
//  listaPok.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class pokemonDetalle;

@interface listaPok : UITableViewController {
}

@property (nonatomic) int elementos;
@property (strong, nonatomic) NSArray *pokemons;
@property (strong, nonatomic) pokemonDetalle *pokemonDetalle;

@end
