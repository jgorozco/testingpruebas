//
//  listaEnt.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class cazadosEnt;

@interface listaEnt : UITableViewController

@property (nonatomic) int elementos;
@property (strong, nonatomic) NSArray *entrenadores;
@property (strong, nonatomic) cazadosEnt *cazadosEnt;

@end
