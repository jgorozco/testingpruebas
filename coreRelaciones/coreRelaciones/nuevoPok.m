//
//  nuevoPok.m
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "nuevoPok.h"
#import "AppDelegate.h"

@interface nuevoPok () 
@end

@implementation nuevoPok

@synthesize nombre, tipo, nivel, estado, ent, elementos, entrenadores, entrenador;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Entrenador" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    self.elementos = [objects count];
    self.entrenadores = objects;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void) guardarDatos 
{   
    BOOL flag = TRUE;
    if ((nombre.text.length == 0) | (tipo.text.length == 0) | (nivel.text.length == 0))
        flag = FALSE;
    if (flag) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *nuevoPokemon;
        nuevoPokemon = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Pokemon"
                        inManagedObjectContext:context];
        [nuevoPokemon setValue:nombre.text forKey:@"nombre"];
        [nuevoPokemon setValue:tipo.text forKey:@"tipo"];
        [nuevoPokemon setValue:nivel.text forKey:@"nivel"]; 
        
        NSString *buscar = entrenador.text;
        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Entrenador" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(nombre CONTAINS[cd] %@)", buscar];
        [request setPredicate:pred];
        NSLog(@"PETICION: %@",request);
        NSError *error2;
        NSArray *objects = [context executeFetchRequest:request error:&error2];
        NSLog(@"BUSQUEDA ENCONTRADA: %@",objects);
        NSManagedObject *trainer = [objects objectAtIndex:0];
        NSLog(@"ENTRENADOR ENCONTRADO: %@",trainer);
        
        [nuevoPokemon setValue:trainer forKey:@"trainer"];
        NSLog(@"Contacto creado con datos: %@, %@, %@, %@",nombre.text,tipo.text,nivel.text,entrenador.text);
        nombre.text = @"";
        tipo.text = @"";
        nivel.text = @"";
        entrenador.text = @"";
        NSError *error;
        [context save:&error];
        estado.text = @"Pokemon almacenado";
    }
    else {
        estado.text = @"Debe rellenar todos los datos";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return elementos;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.ent dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configure the cell...
    NSManagedObject *matches = nil;
    matches = [self.entrenadores objectAtIndex:indexPath.row];    
    NSLog(@"entrenadores encontrados: %@ ",matches);
    NSString *name = [matches valueForKey:@"nombre"];
    cell.textLabel.text = name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int x = indexPath.row;
    NSMutableDictionary *d = [self.entrenadores objectAtIndex:x];
    NSLog(@"VALOR DE D: %@",d);
    entrenador.text = [d valueForKey:@"nombre"];
    
}

@end
