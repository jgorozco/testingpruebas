//
//  entrenadorDetalle.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface entrenadorDetalle : UIViewController

@property (strong, nonatomic) NSMutableDictionary *detailTrainer;

@property (strong, nonatomic) IBOutlet UILabel *nombreT;
@property (strong, nonatomic) IBOutlet UILabel *origenT;
@property (strong, nonatomic) IBOutlet UILabel *victoriasT;

@end
