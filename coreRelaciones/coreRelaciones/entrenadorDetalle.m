//
//  entrenadorDetalle.m
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "entrenadorDetalle.h"

@interface entrenadorDetalle ()

@end

@implementation entrenadorDetalle

@synthesize detailTrainer,nombreT, origenT, victoriasT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Detail Trainer: %@",detailTrainer);
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Valor del DetailItem: %@",self.detailTrainer);
    NSString *no = [self.detailTrainer valueForKey:@"nombre"];
    NSString *or = [self.detailTrainer valueForKey:@"origen"];
    NSString *vi = [self.detailTrainer valueForKey:@"victorias"];
    // Do any additional setup after loading the view from its nib.
    self.nombreT.text = no;
    self.origenT.text = or;
    self.victoriasT.text = vi;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
