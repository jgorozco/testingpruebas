//
//  nuevoPok.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nuevoPok : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    
    UITextField *nombre;
    UITextField *tipo;
    UITextField *nivel; 
    UILabel *estado;
    UILabel *entrenador;
    UITableView *ent;
}

@property (nonatomic) int elementos;
@property (strong, nonatomic) NSArray *entrenadores;
@property (strong, nonatomic) IBOutlet UITextField *nombre;
@property (strong, nonatomic) IBOutlet UITextField *tipo;
@property (strong, nonatomic) IBOutlet UITextField *nivel;
@property (strong, nonatomic) IBOutlet UILabel *estado;
@property (strong, nonatomic) IBOutlet UILabel *entrenador;
@property (strong, nonatomic) IBOutlet UITableView *ent;

- (IBAction)guardarDatos;

@end
