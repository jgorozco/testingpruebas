//
//  vistaPrincipal.h
//  coreRelaciones
//
//  Created by pcoellov on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface vistaPrincipal : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *insertarPokemon;
@property (strong, nonatomic) IBOutlet UIButton *insertarEntrenador;
@property (strong, nonatomic) IBOutlet UIButton *listaPokemon;
@property (strong, nonatomic) IBOutlet UIButton *listaEntrenador;
@property (strong, nonatomic) IBOutlet UIButton *modificarPokemon;

@property (strong, nonatomic) IBOutlet UILabel *labelEncontrado1;
@property (strong, nonatomic) IBOutlet UIButton *boton1Query;
@property (strong, nonatomic) IBOutlet UILabel *labelEncontrado2;
@property (strong, nonatomic) IBOutlet UIButton *boton2Query;
@property (strong, nonatomic) IBOutlet UILabel *labelEncontrado3;
@property (strong, nonatomic) IBOutlet UIButton *boton3Query;

@property (strong, nonatomic) IBOutlet UIButton *botonObject;
@property (strong, nonatomic) IBOutlet UILabel *labelID;
@property (strong, nonatomic) IBOutlet UILabel *prueba;

@end
