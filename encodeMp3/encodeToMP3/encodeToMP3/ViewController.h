//
//  ViewController.h
//  encodeToMP3
//
//  Created by jose garcia orozco on 01/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
- (IBAction)stopsounds:(id)sender;
- (IBAction)encodeBtn:(id)sender;
- (IBAction)listenCaf:(id)sender;
- (IBAction)listenMP3:(id)sender;
@property(nonatomic,retain)AVAudioPlayer *audioPlayer;
@end
