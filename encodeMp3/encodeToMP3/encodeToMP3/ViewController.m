//
//  ViewController.m
//  encodeToMP3
//
//  Created by jose garcia orozco on 01/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize audioPlayer;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)stopsounds:(id)sender {
    if ((audioPlayer)&&(audioPlayer.isPlaying))
    {
    
        [audioPlayer stop];
        audioPlayer=nil;
    }
}

- (IBAction)encodeBtn:(id)sender {
}

- (IBAction)listenCaf:(id)sender {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"music" ofType:@"caf"];
    [self playFile:soundFilePath];
}

- (IBAction)listenMP3:(id)sender {
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"music" ofType:@"mp3"];
    [self playFile:soundFilePath];
}

-(void)playFile:(NSString *)str
{
 /*   [[AVAudioSession sharedInstance] setActive: YES error: nil];
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/music.caf", [[NSBundle mainBundle] resourcePath]]];
    NSData *d=[NSData dataWithContentsOfURL:url];
    NSLog(@"Data?:%d",d.length);
    AVAudioPlayer *audioPlayer1 = [[AVAudioPlayer alloc] initWithData:d error:nil];
    audioPlayer1.numberOfLoops = -1;
    
    [audioPlayer1 play];
*/
  [[AVAudioSession sharedInstance] setActive: YES error: nil];

    NSData *d=[NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
    NSLog(@"DATOS PARA:%@  [%d]",str,d.length);
    audioPlayer=[[AVAudioPlayer alloc]initWithData:d error:nil];
    [audioPlayer prepareToPlay];
    [audioPlayer setVolume: 1.0];
    [audioPlayer play];


}





@end
