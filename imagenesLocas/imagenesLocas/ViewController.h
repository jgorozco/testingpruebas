//
//  ViewController.h
//  imagenesLocas
//
//  Created by pcoellov on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *vistaPrincipal;
@property (strong, nonatomic) IBOutlet UIButton *bonton100200;
@property (strong, nonatomic) IBOutlet UIButton *boton500800;
@property (strong, nonatomic) IBOutlet UIButton *boton20001500;
@property (strong, nonatomic) IBOutlet UIView *vistaShield;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *botoncerrar;
@property (strong, nonatomic) IBOutlet UIButton *botonmaximizar;
@property (strong, nonatomic) IBOutlet UIButton *botonOtraVista;
@property (strong, nonatomic) IBOutlet UIView *otroview;
@property (nonatomic) int boton;
- (IBAction)maximizar:(id)sender;
- (IBAction)cerrar:(id)sender:(int)boton;
- (IBAction)abrir100200:(id)sender;
- (IBAction)abrir500800:(id)sender;
- (IBAction)abrir20001500:(id)sender;
- (IBAction)otravista:(id)sender;

@end
