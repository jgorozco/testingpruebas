//
//  ViewController.m
//  imagenesLocas
//
//  Created by pcoellov on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

// (100x200) http://1.bp.blogspot.com/-_fjARY_F9Rs/TlKQ1w-qEsI/AAAAAAAAAFY/rKaEWn3yew8/s200/relief-anubis.jpg

// (500x800) http://desmotivaciones.es/demots/201104/The_SimpsonsFat_Tony%5B1%5D.png.jpg

// (2000x1500) http://image.lang-8.com/w0_h0/38a8b3e5c788817bcc08a89c8563517252c13fc8.JPG

@synthesize vistaPrincipal;
@synthesize bonton100200;
@synthesize boton500800;
@synthesize boton20001500;
@synthesize vistaShield;
@synthesize imageView;
@synthesize botoncerrar;
@synthesize botonmaximizar;
@synthesize botonOtraVista;
@synthesize boton;
@synthesize otroview; 

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setVistaPrincipal:nil];
    [self setBonton100200:nil];
    [self setBoton500800:nil];
    [self setBoton20001500:nil];
    [self setVistaShield:nil];
    [self setImageView:nil];
    [self setBotoncerrar:nil];
    [self setBotonmaximizar:nil];
    [self setBotonOtraVista:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Comprobar ancho y alto y llamar a la funcion de redimension
    //  funcion de redimension debe ser "universal" (iphone/ipad)
    return YES;
}
- (IBAction)maximizar:(id)sender {
}

- (IBAction)otravista:(id)sender
{   
    [otroview setBackgroundColor:[[UIColor alloc] initWithRed:0.0 green:1.0 blue:0.0 alpha:1.0]];
    [UIView animateWithDuration:3.0
            delay:0.0
            options: UIViewAnimationCurveLinear
            animations:^{
                float iniX = 900;
                float iniY = 650;
                CGRect nuevo = CGRectMake(iniX,iniY,100,100);
                [otroview setFrame:nuevo];
                [otroview setBackgroundColor:[[UIColor alloc] initWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]];
                [self.vistaPrincipal setFrame:otroview.frame];
                    } 
                completion:^(BOOL finished){
                }];
}

- (IBAction)cerrar:(id)sender:(int)boton {
    [UIView animateWithDuration:3.0
            delay:0.0
            options: UIViewAnimationCurveLinear
            animations:^{
                CGRect bot;
                if (self.boton == 1) {
                    bot = self.bonton100200.frame;
                }
                if (self.boton == 2) {
                    bot = self.boton500800.frame;
                }
                if (self.boton == 3) {
                    bot = self.boton20001500.frame;
                }

                [self.vistaShield setBackgroundColor:[[UIColor alloc] initWithRed:1.0 green:1.0 blue:1.0 alpha:0.0]];
                [self.imageView setFrame:bot];
                    } 
                completion:^(BOOL finished){
                    [self.vistaShield setHidden:YES];
                    [self.imageView setHidden:YES];
                    [self.botoncerrar setHidden:YES];
                    [self.botonmaximizar setHidden:YES];
                }];
}

- (IBAction)abrir100200:(id)sender {
    self.boton = 1;
    UIView *botonframe = sender;
    float iniW = botonframe.frame.size.width;
    float iniH = botonframe.frame.size.height;
    float iniX = botonframe.frame.origin.x;
    float iniY = botonframe.frame.origin.y;
    [botonframe setBackgroundColor:[[UIColor alloc] initWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    CGRect inicial = CGRectMake(iniX, iniY, iniW, iniH);
    [self.imageView setFrame:inicial];
    [self.vistaShield setBackgroundColor:[[UIColor alloc]initWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    [self.vistaShield setHidden:NO];
    [self.botoncerrar setHidden:NO];
    [self.botonmaximizar setHidden:NO];
    [self.imageView setHidden:NO];
    NSData *datossmall = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://1.bp.blogspot.com/-_fjARY_F9Rs/TlKQ1w-qEsI/AAAAAAAAAFY/rKaEWn3yew8/s200/relief-anubis.jpg"]];
    UIImage *small = [[UIImage alloc] initWithData:datossmall];
    [self.imageView setImage:small];
    [UIView animateWithDuration:3.0
            delay:0.0
            options: UIViewAnimationCurveEaseIn
            animations:^{
                float w = 100.0;
                float h = 200.0;
                float x = (1024-w)/2;
                float y = (768-h)/2;
                CGRect nuevo = CGRectMake(x, y, w, h);
                [self.vistaShield setBackgroundColor:[[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:0.5]];
                [self.imageView setFrame:nuevo];
            } 
            completion:^(BOOL finished){
                NSLog(@"Done!");
            }];
}

- (IBAction)abrir500800:(id)sender {
    self.boton = 2;
    UIView *botonframe = sender;
    float iniW = botonframe.frame.size.width;
    float iniH = botonframe.frame.size.height;
    float iniX = botonframe.frame.origin.x;
    float iniY = botonframe.frame.origin.y;
    [botonframe setBackgroundColor:[[UIColor alloc] initWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    CGRect inicial = CGRectMake(iniX, iniY, iniW, iniH);
    [self.imageView setFrame:inicial];
    [self.vistaShield setBackgroundColor:[[UIColor alloc]initWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    [self.vistaShield setBackgroundColor:[[UIColor alloc]initWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    [self.vistaShield setHidden:NO];
    [self.botoncerrar setHidden:NO];
    [self.botonmaximizar setHidden:NO];
    [self.imageView setHidden:NO];
    NSData *datosmedium = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://desmotivaciones.es/demots/201104/The_SimpsonsFat_Tony%5B1%5D.png.jpg"]];
    UIImage *medium = [[UIImage alloc] initWithData:datosmedium];
    [self.imageView setImage:medium];
    [UIView animateWithDuration:3.0
                          delay:0.0
                        options: UIViewAnimationCurveEaseInOut
                     animations:^{
                         float w = 500.0;
                         float h = 800.0;
                         float x = (1024-500)/2;
                         float y = 0;
                         CGRect nuevo = CGRectMake(x,y,w,h);
                         [self.vistaShield setBackgroundColor:[[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:0.5]];
                         [self.imageView setFrame:nuevo];
                     } 
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
}

- (IBAction)abrir20001500:(id)sender {
    self.boton = 3;
    UIView *botonframe = sender;
    float iniW = botonframe.frame.size.width;
    float iniH = botonframe.frame.size.height;
    float iniX = botonframe.frame.origin.x;
    float iniY = botonframe.frame.origin.y;
    [botonframe setBackgroundColor:[[UIColor alloc] initWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    CGRect inicial = CGRectMake(iniX, iniY, iniW, iniH);
    [self.imageView setFrame:inicial];
    [self.vistaShield setBackgroundColor:[[UIColor alloc]initWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    [self.vistaShield setHidden:NO];
    [self.botoncerrar setHidden:NO];
    [self.imageView setHidden:NO];
    NSData *datosbig = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://image.lang-8.com/w0_h0/38a8b3e5c788817bcc08a89c8563517252c13fc8.JPG"]];
    UIImage *big= [[UIImage alloc] initWithData:datosbig];
    [self.imageView setImage:big];
    [UIView animateWithDuration:1.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         float maxW = (1024/(16/9));
                         float maxH = (768/(16/9));
                         float x = 0;
                         float y = 0;
                         CGRect nuevo = CGRectMake(x,y,maxW,maxH);
                         [self.vistaShield setBackgroundColor:[[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:0.5]];
                         [self.imageView setFrame:nuevo];
                     } 
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
}
@end
