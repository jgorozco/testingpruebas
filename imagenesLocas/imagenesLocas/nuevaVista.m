//
//  nuevaVista.m
//  imagenesLocas
//
//  Created by pcoellov on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "nuevaVista.h"

@interface nuevaVista ()

@end

@implementation nuevaVista

@synthesize otraVista;
@synthesize otroview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSData *datos = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://store.manutd.com/stores/product_images/mufc-55671.jpg"]];
    UIImage *imagen = [[UIImage alloc] initWithData:datos];
    [self.otroview setImage:imagen];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
