//
//  ViewController.m
//  ejemplo Admob
//
//  Created by jose garcia orozco on 31/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    bannerView_ = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
              
                   /*
                   initWithFrame:CGRectMake(0.0,
                                            self.view.frame.size.height -
                                            GAD_SIZE_320x50.height,
                                            GAD_SIZE_320x50.width,
                                            GAD_SIZE_320x50.height)];
                    ç*/
    
    // Especificar el "identificador de bloque" del anuncio. Se trata del ID de editor de AdMob.
    NSArray *cuentasAdmob=[NSArray arrayWithObjects:@"a1504120a9469dc",@"a111111111", nil];
    
    int number = (arc4random()%cuentasAdmob.count); //Generates Number from 1 to 100.
    bannerView_.adUnitID =[cuentasAdmob objectAtIndex:number];// @"a1504120a9469dc";
    
    // Hay que comunicar al módulo de tiempo de ejecución el UIViewController que debe restaurar después de llevar
    // al usuario donde vaya el anuncio y añadirlo a la jerarquía de vistas.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];
    
    // Iniciar una solicitud genérica para cargarla con un anuncio.
    GADRequest *r = [[GADRequest alloc] init];
 //   r.testing = YES;
    [bannerView_ loadRequest:r];

    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
