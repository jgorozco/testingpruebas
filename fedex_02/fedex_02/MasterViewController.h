//
//  MasterViewController.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "frwk.h"
#import "NSLoadedProtocol.h"
@class DetailViewController;


@interface MasterViewController : UITableViewController <NSLoadedProtocol>
{

    NSArray *objetosToShow;
    BOOL p1,p2;

}
@property (strong, nonatomic) DetailViewController *detailViewController;

- (IBAction)reloadBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;

@end
