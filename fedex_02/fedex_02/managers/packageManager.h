//
//  packageManager.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSLoadedProtocol.h"
#define URL_PACK @"http://feverup.com/3as3224jksi2342/packages.json"
@interface packageManager : NSObject
{
    NSArray *countryCodes;
    BOOL justLoaded;
    NSObject<NSLoadedProtocol> *handler;
}
-(packageManager *)init:(NSObject<NSLoadedProtocol> *)myhandler;

-(void)loadPackageTable;

-(NSArray *)getPackagesForCountryCode:(NSString *)ccode;
-(NSArray *)getAllContryCodes;
-(BOOL)isLoaded;
@end
