//
//  frwk.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSLoadedProtocol.h"
#import "conversionManager.h"
#import "packageManager.h"
@interface frwk : NSObject
@property conversionManager *cManager;
@property packageManager *pManager;
+(frwk *)getInstance;
-(frwk *)init;
-(void)loadconversionManager:(NSObject<NSLoadedProtocol> *)hadler;
-(void)loadpackageManager:(NSObject<NSLoadedProtocol> *)hadler;

@end
