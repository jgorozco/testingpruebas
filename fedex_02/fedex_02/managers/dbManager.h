//
//  dbManager.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TABLE_CON @"Conversion"
#define TABLE_PAC @"Packages"
@interface dbManager : NSObject

+(void)addConversion:(NSDictionary *)convDict;
+(NSArray *)getConversionFrom:(NSString *)from;
+(NSArray *)getConversionTo:(NSString *)to;
+(NSArray *)getConversionFrom:(NSString *)from too:(NSString *)to;

+(void)addPackage:(NSDictionary *)packDict;
+(NSArray *)addPackages:(NSArray *)packDict;
+(NSArray *)getPackagesFor:(NSString *)countryCode;

+(BOOL)hasData:(NSString *)table;


@end
