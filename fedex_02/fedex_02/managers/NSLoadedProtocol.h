//
//  NSLoadedProtocol.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NSLoadedProtocol <NSObject>
-(void)justFinish:(NSString *)process;
@end
