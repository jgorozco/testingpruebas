//
//  conversionManager.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "conversionManager.h"
#import "dbManager.h"
@implementation conversionManager


-(conversionManager *)init:(NSObject<NSLoadedProtocol> *)myhandler
{
    handler=myhandler;
    return self;
}
-(BOOL)isLoaded{
    if ([dbManager hasData:TABLE_CON]){
        NSLog(@"No recargamos-CON");
        justLoaded=YES;
    }
    return justLoaded;
}
-(void)loadConversionTable{
    NSURL *url=[NSURL URLWithString:URL_CON];
    NSURLRequest *urlReq=[NSURLRequest requestWithURL:url];
    NSError        *error = nil;
    NSURLResponse  *response = nil;
    NSData *data=[NSURLConnection sendSynchronousRequest:urlReq
                                       returningResponse:&response
                                                   error:&error];
    if (error==nil)
    {
        NSArray *decodedResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        for (NSDictionary *d in decodedResponse)
        {
            [dbManager addConversion:d];
        }
    }else {
        NSLog(@"LOGIN ERROR:%@",error.localizedDescription);
    }

    
    justLoaded=YES;
    [handler justFinish:@"conversionManager"];
}

-(float)getConversionRate:(NSString *)from toMes:(NSString *)to ammount:(float)weight{
    //TODO una vez hecho, cachearla en el userdeaults!!!!
    if (!justLoaded)
        return 0.0;
    
    NSArray *getConv=[dbManager getConversionFrom:from too:to];
    if ((getConv)&&(getConv.count>0))
    {
        NSString *conversionstr=[[getConv objectAtIndex:0] objectForKey:@"rate"];
        float conversion=[conversionstr floatValue];
        return weight*conversion;
    }else{//empezamos a buscar... no es demasiado eficiente lo se, seria mejor dijkstra
        NSMutableDictionary *matrizLlegada=[[NSMutableDictionary alloc] init];
        NSMutableSet *gotonodes=[[NSMutableSet alloc] init];
        NSMutableSet *outer=[[NSMutableSet alloc] init];
        [outer addObject:from];
        NSArray *getConv=[dbManager getConversionFrom:from];
        for (NSDictionary *conversion in getConv) {
            [matrizLlegada setObject:[conversion objectForKey:@"rate"] forKey:[conversion objectForKey:@"to"]];
        }
        BOOL encontrado=NO;
        while (!encontrado) {
            for (NSString *k in matrizLlegada.allKeys)
            {
                gotonodes= [NSMutableSet setWithArray:[dbManager getConversionFrom:k]];
                for (NSString *s in outer)
                {//eliminamos los ya visitados
                    [gotonodes removeObject:s];
                }
                if (gotonodes.count==0)
                {
                    encontrado=YES;
                    return 0.0;
                }
                float tempFloat=[[matrizLlegada objectForKey:k] floatValue];
                for (NSDictionary *d in gotonodes){
                    if ([[d objectForKey:@"to"]isEqualToString:to])
                    {
                        encontrado=YES;
                        return tempFloat*[[d objectForKey:@"rate"] floatValue];
                    }else{
                        [outer addObject:[d objectForKey:@"to"]];
                        NSString *s=[NSString stringWithFormat:@"%f",tempFloat*[[d objectForKey:@"rate"] floatValue]];
                        [matrizLlegada setObject:s forKey:[d objectForKey:@"to"]];
                    }
                }
            }
        }
    }
        
        
     /*   NSArray *getConv=[dbManager getConversionTo:to];
        BOOL encontrado=NO;
        NSMutableDictionary *buscando=[[NSMutableDictionary alloc] init];
        for (NSDictionary *d in getConv){
            [buscando setObject:[d objectForKey:@"rate"] forKey:@"from"];
        }
        while (!encontrado) {
            NSMutableDictionary *buscandoTemp=[NSMutableDictionary dictionaryWithDictionary:buscando];
            for (NSString *to1 in buscandoTemp.allKeys)
            {
                 NSArray *getConv=[dbManager getConversionTo:to1];
                float k=[[buscandoTemp objectForKey:to1] floatValue];
              //  [buscando removeObjectForKey:to1];
                
                for (NSDictionary *d in getConv)
                {
                    float jumpvalue=[[d objectForKey:@"rate"] floatValue];
                    if ([[d objectForKey:@"from"] isEqualToString:from])
                    {
                        encontrado=YES;
                        return k*jumpvalue*weight;
                    }else{
                        NSString *value=[NSString stringWithFormat:@"%f",k*jumpvalue];
                        [buscando setObject:value forKey:@"from"];
                    }
                }
            }
        }
    }
    */
    return 0.0;
}


@end
