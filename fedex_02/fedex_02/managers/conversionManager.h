//
//  conversionManager.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSLoadedProtocol.h"
#define URL_CON @"http://feverup.com/3as3224jksi2342/conversions.json"
@interface conversionManager : NSObject
{
    BOOL justLoaded;
    NSObject<NSLoadedProtocol> *handler;

}
-(conversionManager *)init:(NSObject<NSLoadedProtocol> *)myhandler;

-(void)loadConversionTable;

-(float)getConversionRate:(NSString *)from toMes:(NSString *)to ammount:(float)weight;
-(BOOL)isLoaded;

@end
