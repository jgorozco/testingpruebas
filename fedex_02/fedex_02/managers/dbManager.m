//
//  dbManager.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "dbManager.h"
#import "AppDelegate.h"
@implementation dbManager

+(void)addConversion:(NSDictionary *)convDict{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *conv = [NSEntityDescription
                                        insertNewObjectForEntityForName:TABLE_CON
                                        inManagedObjectContext:context];
    [conv setValue:[convDict objectForKey:@"from"] forKey:@"from"];
    [conv setValue:[convDict objectForKey:@"to"] forKey:@"to"];
    [conv setValue:[convDict objectForKey:@"rate"] forKey:@"rate"];
    NSError *error;
    [context save:&error];
    if (error)
        NSLog(@"Error aniadiendo la tupla:%@",convDict);


}
+(BOOL)hasData:(NSString *)table{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:table inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    NSLog(@"Num elems:%@=%d",table,objects.count);
    return objects.count>0;


}

+(NSArray *)getConversionFrom:(NSString *)from{
    return [self getConversion:@"from" data:from];
}
+(NSArray *)getConversionTo:(NSString *)to{
    return [self getConversion:@"to" data:to];
}
+(NSArray *)getConversionFrom:(NSString *)from too:(NSString *)to{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:TABLE_CON inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSMutableArray *parr = [NSMutableArray array];
    
    // NSString *predicadobuscar=[NSString stringWithFormat:@"(%@ CONTAINS %@) OR (%@ CONTAINS %@)",DIC_CLIENTE_N, filter,DIC_RAZSOCIAL_N,filter];
    NSString *predicadobuscar=[NSString stringWithFormat:@"(%@='%@')",@"from",from];
    NSPredicate *pred = [NSPredicate predicateWithFormat:predicadobuscar];
    [parr addObject:pred];
    NSString *predicadobuscarw=[NSString stringWithFormat:@"(%@='%@')",@"to",to];
    NSPredicate *predw2= [NSPredicate predicateWithFormat:predicadobuscarw];
    [parr addObject:predw2];
    //  NSCompoundPredicate *cpred=;    //[NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:pred, nil]];
    
    [request setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:parr]];
    
    
    NSError *error;
    NSArray *objetos = [context executeFetchRequest:request error:&error];
    if (error)
        NSLog(@"ERR:%@",error.localizedDescription);
    if (!objetos)
    {
        return [[NSArray alloc] init];
    }
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    NSDictionary *tempDic;
    for (NSManagedObject *o in objetos)
    {
   //     NSLog(@"DDD:%@",o);
        tempDic=[o dictionaryWithValuesForKeys:[NSArray arrayWithObjects:@"rate",@"from",@"to", nil]];
        NSLog(@"ALLKEYS:%d",tempDic.allKeys.count);
        if (tempDic.allKeys.count==3)
            [arr addObject:tempDic];
        
    }
    return arr;

}

+(void)removeAllPacks
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TABLE_PAC inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
    if (![context save:&error]) {
        NSLog(@"Error deleting - error:%@",error.localizedDescription);
    }
    
}


+(NSArray *)getConversion:(NSString *)type data:(NSString *)data{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:TABLE_CON inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSString *predicadobuscar=[NSString stringWithFormat:@"(%@='%@')",type, data];
    //   NSLog(@"111:BUSCANDO::::[%@]",predicadobuscar);
    NSPredicate *pred = [NSPredicate predicateWithFormat:predicadobuscar];
    [request setPredicate:pred];
    NSError *error;
    NSArray *objetos = [context executeFetchRequest:request error:&error];
    if (!objetos)
    {
        return [[NSArray alloc] init];
    }
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    NSMutableDictionary *tempDic;
    for (NSManagedObject *o in objetos)
    {
        tempDic=[[NSMutableDictionary alloc] init];
        [tempDic setObject:[o valueForKey:@"from"] forKey:@"from"];
        [tempDic setObject:[o valueForKey:@"to"] forKey:@"to"];
        [tempDic setObject:[o valueForKey:@"rate"] forKey:@"rate"];
        [arr addObject:tempDic];

    }
    return arr;

}

+(NSArray *)addPackages:(NSArray *)packDict{
    [self removeAllPacks];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSMutableSet *set=[[NSMutableSet alloc] init];
    int i=0;
    for (NSDictionary *d in packDict)
    {
        NSManagedObject *conv = [NSEntityDescription
                                 insertNewObjectForEntityForName:TABLE_PAC
                                 inManagedObjectContext:context];
        [conv setValue:[d objectForKey:@"destination"] forKey:@"destination"];
        [conv setValue:[d objectForKey:@"id"] forKey:@"id"];
        [conv setValue:[d objectForKey:@"weight"] forKey:@"weight"];
        [conv setValue:[d objectForKey:@"measure"] forKey:@"measure"];
        [set addObject:[d objectForKey:@"destination"]];
    //    NSLog(@"a:%d",i);
        i=i+1;
    }
    NSError *error;
    [context save:&error];
    if (error)
        NSLog(@"Error aniadiendo la tupla:%@",packDict);
    return set.allObjects;
}

+(void)addPackage:(NSDictionary *)packDict{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *conv = [NSEntityDescription
                             insertNewObjectForEntityForName:TABLE_PAC
                             inManagedObjectContext:context];
    [conv setValue:[packDict objectForKey:@"destination"] forKey:@"destination"];
    [conv setValue:[packDict objectForKey:@"id"] forKey:@"id"];
    [conv setValue:[packDict objectForKey:@"weight"] forKey:@"weight"];
    [conv setValue:[packDict objectForKey:@"measure"] forKey:@"measure"];
    NSError *error;
    [context save:&error];
    if (error)
        NSLog(@"Error aniadiendo la tupla:%@",packDict);


}
+(NSArray *)getPackagesFor:(NSString *)countryCode{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:TABLE_PAC inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSString *predicadobuscar=[NSString stringWithFormat:@"(%@='%@')",@"destination", countryCode];
    NSLog(@"111:BUSCANDO::::[%@]",predicadobuscar);
    NSPredicate *pred = [NSPredicate predicateWithFormat:predicadobuscar];
    [request setPredicate:pred];
    NSError *error;
    NSArray *objetos = [context executeFetchRequest:request error:&error];
    if (!objetos)
    {
        return [[NSArray alloc] init];
    }
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    NSMutableDictionary *tempDic;
    for (NSManagedObject *o in objetos)
    {
        tempDic=[[NSMutableDictionary alloc] init];
        [tempDic setObject:[o valueForKey:@"destination"] forKey:@"destination"];
        [tempDic setObject:[o valueForKey:@"id"] forKey:@"id"];
        [tempDic setObject:[o valueForKey:@"weight"] forKey:@"weight"];
        [tempDic setObject:[o valueForKey:@"measure"] forKey:@"measure"];
        [arr addObject:tempDic];
        
    }
    return arr;



}



@end
