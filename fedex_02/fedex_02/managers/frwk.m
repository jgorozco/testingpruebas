//
//  frwk.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "frwk.h"

@implementation frwk

static frwk *singleFrwk = nil;


@synthesize cManager,pManager;
+ (frwk *)getInstance
{
    @synchronized(self)
    {
        if (!singleFrwk)
            singleFrwk = [[frwk alloc] init];
        
        return singleFrwk;
    }
}
-(frwk *)init{
    pManager=NULL;
    cManager=NULL;
    
    return self;

}

-(void)loadconversionManager:(NSObject<NSLoadedProtocol> *)hadler{
    if (!cManager){
    cManager=[[conversionManager alloc] init:hadler];
   //     [cManager loadConversionTable];
    }

}
-(void)loadpackageManager:(NSObject<NSLoadedProtocol> *)hadler{
    if (!pManager){
        pManager=[[packageManager alloc] init:hadler];
   //     [pManager loadPackageTable];
    }

}


@end
