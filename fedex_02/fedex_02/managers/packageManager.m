//
//  packageManager.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "packageManager.h"
#import "dbManager.h"
@implementation packageManager



-(packageManager *)init:(NSObject<NSLoadedProtocol> *)myhandler{
    handler=myhandler;
    countryCodes=[[NSArray alloc] init];
    justLoaded=NO;
    return self;

}
-(BOOL)isLoaded{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"countrycodes"]){
        NSLog(@"No recargamos-pack");
        justLoaded=YES;
    }

    return justLoaded;
}


-(void)loadPackageTable{
    NSURL *url=[NSURL URLWithString:URL_PACK];
    NSURLRequest *urlReq=[NSURLRequest requestWithURL:url];
    NSError        *error = nil;
    NSURLResponse  *response = nil;
    NSData *data=[NSURLConnection sendSynchronousRequest:urlReq
                                       returningResponse:&response
                                                   error:&error];
    NSLog(@"recibidos datos:%d",data.length);
    if (error==nil)
    {
        NSArray *decodedResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"objetos recibidos:%d",decodedResponse.count);
        countryCodes=[dbManager addPackages:decodedResponse];
        [[NSUserDefaults standardUserDefaults] setValue:countryCodes forKey:@"countrycodes"];

    }else {
        NSLog(@"LOGIN ERROR:%@",error.localizedDescription);
    }
    
    
    justLoaded=YES;
    [handler justFinish:@"packageManager"];



}

-(NSArray *)getPackagesForCountryCode:(NSString *)ccode{
    if (!justLoaded)
        return [[NSArray alloc]init];
    return [dbManager getPackagesFor:ccode];
}
-(NSArray *)getAllContryCodes{
    if (!justLoaded)
        return [[NSArray alloc]init];
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"countrycodes"];
}
@end
