//
//  DetailViewController.h
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    NSArray *paquetesShow;
}
@property (strong, nonatomic) NSString * detailItem;
@property (strong, nonatomic) IBOutlet UILabel *labelAviones;
@property (strong, nonatomic) IBOutlet UILabel *labelKgs;
@property (strong, nonatomic) IBOutlet UITableView *mitablaView;
@property (strong, nonatomic) IBOutlet UILabel *labelTotal;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
-(void)loadDataOfPackages:(NSString *)str;
@end
