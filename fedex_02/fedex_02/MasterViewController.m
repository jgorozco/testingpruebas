//
//  MasterViewController.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "frwk.h"
@interface MasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MasterViewController
@synthesize activityLoading;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Master", @"Master");
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    p1=p2=NO;
    objetosToShow=[[NSArray alloc] init];
    [self performSelectorInBackground:@selector(loadingPhase) withObject:nil];
   

}

-(void)loadingPhase{

    [[frwk getInstance] loadconversionManager:self];
    if (![[frwk getInstance].cManager isLoaded]){
         [[frwk getInstance].cManager loadConversionTable];
    }else{
        p1=YES;
    }
    
    [[frwk getInstance] loadpackageManager:self];
    [[frwk getInstance].pManager loadPackageTable];
  /*  if (![[frwk getInstance].pManager isLoaded]){
        NSLog(@"Cargando paquetes");
        [[frwk getInstance].pManager loadPackageTable];
    }else{
        p2=YES;
        objetosToShow=[[frwk getInstance].pManager getAllContryCodes];
        [self.tableView reloadData];
    }
    */
    [self finished:@""];
    
}
-(void)justFinish:(NSString *)process{
    [self performSelectorOnMainThread:@selector(finished:) withObject:process waitUntilDone:NO];
}
-(void)finished:(NSString *)process
{
    if ([process isEqualToString:@"conversionManager"]){
        p1=YES;
        NSLog(@"Paso 1 terminado");
    }
    if ([process isEqualToString:@"packageManager"]){
        p2=YES;
        objetosToShow=[[frwk getInstance].pManager getAllContryCodes];
        [self.tableView reloadData];
        NSLog(@"Paso 2 terminado");

    }
    if (p1&&p2)
    {
        [activityLoading stopAnimating];
    }
}


- (void)viewDidUnload
{
    [self setActivityLoading:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [objetosToShow count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    [[cell textLabel]setText:[objetosToShow objectAtIndex:indexPath.row]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController *detailViewController2 = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];

    NSString *object = [objetosToShow objectAtIndex:indexPath.row];
    [detailViewController2 loadDataOfPackages:object];
    [self.navigationController pushViewController:detailViewController2 animated:YES];
}

#pragma mark - Fetched results controller


- (IBAction)reloadBtn:(id)sender {
}
@end
