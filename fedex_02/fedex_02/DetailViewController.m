//
//  DetailViewController.m
//  fedex_02
//
//  Created by jose garcia orozco on 23/09/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "DetailViewController.h"
#import "frwk.h"
@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item
@synthesize labelAviones;
@synthesize labelKgs;
@synthesize mitablaView;
@synthesize labelTotal;


-(void)loadDataOfPackages:(NSString *)str{
    _detailItem=str;

}
- (void)configureView
{
    // Update the user interface for the detail item.
    paquetesShow=[[frwk getInstance].pManager getPackagesForCountryCode:_detailItem];
    [labelTotal setText:[NSString stringWithFormat:@"Total:%d",paquetesShow.count]];
    float pesoTotal=0.0;
    for (NSDictionary *d in paquetesShow){
        NSString  *formatoPeso=[d objectForKey:@"measure"];
        float peso=[[d objectForKey:@"weight"] floatValue];
        if ([formatoPeso isEqualToString:@"KG"])
        {
            pesoTotal=peso+pesoTotal;
        }else{
            pesoTotal=pesoTotal+[[frwk getInstance].cManager getConversionRate:formatoPeso toMes:@"KG" ammount:peso];
     //       NSLog(@"llevamos:%f con:%@",pesoTotal,[d objectForKey:@"id"]);
        }
    }
    [labelKgs setText:[NSString stringWithFormat:@"Peso total:%f kg",pesoTotal]];
    int aviones=(int)(pesoTotal/1000);
    [labelAviones setText:[NSString stringWithFormat:@"AVIONES:%d",aviones]];
  //  NSLog(@"--->%@",paquetesShow);
    [mitablaView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [self setLabelAviones:nil];
    [self setLabelKgs:nil];
    [self setMitablaView:nil];
    [self setLabelTotal:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
    }
    return self;
}
			



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [paquetesShow count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [[cell textLabel]setText:[[paquetesShow objectAtIndex:indexPath.row]objectForKey:@"id" ]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}




@end
