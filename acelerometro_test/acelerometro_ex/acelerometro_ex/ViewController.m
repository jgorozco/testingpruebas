//
//  ViewController.m
//  acelerometro_ex
//
//  Created by jose garcia orozco on 28/08/12.
//  Copyright (c) 2012 jose garcia orozco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize accelerometer;
@synthesize acelx;
@synthesize acely;
@synthesize acelz;
@synthesize incx;
@synthesize incy;
@synthesize incz;
@synthesize lblAcelx;
@synthesize lblAcely;
@synthesize lblAcelz;
@synthesize lblInclinacionx;
@synthesize lblInclinaciony;
@synthesize lblInclinacionz;
@synthesize isSaking;
- (void)viewDidLoad
{
    [super viewDidLoad];
    intx=0.0;
    inty=0.0;
    intz=0.0;
    [self usingAllMotion];

    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)usingAllMotion
{
    acumuladox=0;
    acumuladoy=0;
  self.motionManager = [[CMMotionManager alloc] init];
  self.motionManager.deviceMotionUpdateInterval = 0.05f;
    if (self.motionManager.deviceMotionAvailable) {
        
        NSLog(@"Device Motion Available");
        [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue]
                                           withHandler: ^(CMDeviceMotion *motion, NSError *error){
                                               //CMAttitude *attitude = motion.attitude;
                                               //NSLog(@"rotation rate = [%f, %f, %f]", attitude.pitch, attitude.roll, attitude.yaw);
                                               [self performSelectorOnMainThread:@selector(handleDeviceMotion:) withObject:motion waitUntilDone:YES];
                                               
                                           }];
        //[motionManager startDeviceMotionUpdates];
        
        
    } else {
        NSLog(@"No device motion on device.");
        [self setMotionManager:nil];
    }
}
- (void)handleDeviceMotion:(CMDeviceMotion*)motion{
    CMAttitude *attitude = motion.attitude;
    
    CMAcceleration acceleration = motion.userAcceleration;    
    CMRotationRate rotationRate = motion.rotationRate;
    float PI = 3.14159265;
    float yaw = attitude.yaw * 180/PI;
    float pitch = attitude.pitch * 180/PI;
    float roll = attitude.roll * 180/PI;
    /*Son calculados*/
    lblInclinacionx.text=[[NSString alloc] initWithFormat:@"%.02f",yaw];
    self.incx.progress = (yaw/180.0)+0.5;
    lblInclinaciony.text=[[NSString alloc] initWithFormat:@"%.02f",pitch];
    self.incy.progress = (pitch/180.0)+0.5;
    lblInclinacionz.text=[[NSString alloc] initWithFormat:@"%.02f",roll];
    
    self.incz.progress = (roll/180.0)+0.5;
    lblAcelx.text = [NSString stringWithFormat:@"%@%f", @"X: ", acceleration.x];
    lblAcely.text = [NSString stringWithFormat:@"%@%f", @"Y: ", acceleration.y];
    lblAcelz.text = [NSString stringWithFormat:@"%@%f", @"Z: ", acceleration.z];
    self.acelx.progress = acceleration.x*0.5+0.5;
    self.acely.progress =  acceleration.y*0.5+0.5;
    self.acelz.progress =  acceleration.z*0.5+0.5;
    
    CGRect make=self.botonMover.frame;
    acumuladox=acumuladox+attitude.pitch;
    acumuladoy=acumuladoy+attitude.roll;

    float posMaxX=make.origin.x+acumuladox;
    float posMaxY=make.origin.y+acumuladoy;
    //comprobamos los limites y lo volvemos a dejar dentro.
    if (posMaxX<0)
        posMaxX=0;
    if (posMaxY<0)
        posMaxY=0;
    if (posMaxX>500)
        posMaxX=500;
    if (posMaxY>500)
        posMaxY=500;
    NSLog(@"ACx:%f ACy:%f",acumuladox,acumuladoy);    
    NSLog(@"MAXx:%f MAXy:%f",posMaxX,posMaxY);    
    CGRect make2=CGRectMake(posMaxX,
                            posMaxY,
                            make.size.width,
                            make.size.height);
    
    
    self.botonMover.frame=make2;
//    NSLog(@"rotation rate = [Pitch: %f, Roll: %f, Yaw: %f]", attitude.pitch, attitude.roll, attitude.yaw);
//    NSLog(@"motion.rotationRate = %f", rotationRate.x);


}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}
- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"Evento:Begin:%@",event);
    if (event.type == UIEventSubtypeMotionShake) {
        [isSaking setOn:YES animated:YES];
    }

}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"Evento:end:%@",event);
    [isSaking setOn:NO animated:YES];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}


-(void)usingUIAccelerometerDelegate
{
    self.accelerometer = [UIAccelerometer sharedAccelerometer];
    self.accelerometer.updateInterval = 0.05f;
    self.accelerometer.delegate = self;
    

}

-(void)usingMotionManager
{
    self.motionManager = [[CMMotionManager alloc] init];
    
    
    //Gyroscope
    if([self.motionManager isGyroAvailable])
    {
        /* Start the gyroscope if it is not active already */
        if([self.motionManager isGyroActive] == NO)
        {
            /* Update us 2 times a second */
            [self.motionManager setGyroUpdateInterval:0.05f];
            
            /* And on a handler block object */
            
            /* Receive the gyroscope data on this block */
            [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue mainQueue]
                                            withHandler:^(CMGyroData *gyroData, NSError *error)
             {
                 NSString *x = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.x];
                 self.lblInclinacionx.text = x;
                 self.incx.progress = gyroData.rotationRate.x*0.5+0.5;
                 NSString *y = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.y];
                 self.lblInclinaciony.text = y;
                 self.incy.progress = gyroData.rotationRate.y*0.5+0.5;
                 NSString *z = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.z];
                 self.lblInclinacionz.text = z;
                 self.incz.progress = gyroData.rotationRate.z*0.5+0.5;
             }];
        }
    }
    else
    {
        NSLog(@"Gyroscope not Available!");
    }

}

- (void)viewDidUnload
{
    [self setAcelx:nil];
    [self setAcely:nil];
    [self setAcelz:nil];
    [self setIncx:nil];
    [self setIncy:nil];
    [self setIncz:nil];
    [self setLblAcelx:nil];
    [self setLblAcely:nil];
    [self setLblAcelz:nil];
    [self setLblInclinacionx:nil];
    [self setLblInclinaciony:nil];
    [self setLblInclinacionz:nil];
    [self setIsSaking:nil];
    [self setBotonMover:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    //deahabilitar el giro del ipad
    /*
    if (interfaceOrientation != UIInterfaceOrientationLandscapeLeft)
        return YES;
    if (interfaceOrientation != UIInterfaceOrientationLandscapeRight)
        return YES;*/
    return NO;
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
    lblAcelx.text = [NSString stringWithFormat:@"%@%f", @"X: ", acceleration.x];
    lblAcely.text = [NSString stringWithFormat:@"%@%f", @"Y: ", acceleration.y];
    lblAcelz.text = [NSString stringWithFormat:@"%@%f", @"Z: ", acceleration.z];
    self.acelx.progress = ABS(acceleration.x);
    self.acely.progress = ABS(acceleration.y);
    self.acelz.progress = ABS(acceleration.z);

}

@end
