//
//  ViewController.m
//  facebookupload
//
//  Created by jose garcia on 24/04/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize datos;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setDatos:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)btn3:(id)sender {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Download a sample video
    // Download a sample photo
    NSURL *url = [NSURL URLWithString:@"http://www.vadejuegos.com/imagenes/2012/04/24/sonika820-re.jpg"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img  = [[UIImage alloc] initWithData:data];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"mensaje en la foto", @"message",
                                   @"titulete de foto", @"title",
                                   @"descripcion de foto", @"description",
                                   img, @"picture",
                                   nil];    
    [[delegate facebook] requestWithGraphPath:@"me/photos"
                                    andParams:params
                                andHttpMethod:@"POST"
                                  andDelegate:self];

}

- (IBAction)btn1:(id)sender {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *extendedPermissions = [[NSArray alloc] initWithObjects:@"user_likes",
                                    @"user_videos",
                                    @"publish_stream",
                                    @"user_photos",
                                    nil];
    [[delegate facebook] authorize:extendedPermissions];

    //pedir permisos
    
    
}

- (IBAction)btn2:(id)sender {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    // Download a sample video
    NSURL *url = [NSURL URLWithString:@"https://developers.facebook.com/attachment/sample.mov"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   data, @"video.mov",
                                   @"video/quicktime", @"contentType",
                                   @"Video Test Title", @"title",
                                   @"Video Test Description", @"description",
								   nil];
	[[delegate facebook] requestWithGraphPath:@"me/videos"
                                    andParams:params
                                andHttpMethod:@"POST"
                                  andDelegate:self];
}
- (void)requestLoading:(FBRequest *)request
{
    NSLog(@"received response::requestLoading:%@",request.description);

    
}


- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"received response::didReceiveResponse:%@",response);
}


- (void)request:(FBRequest *)request didLoad:(id)result {
    NSLog(@"received request:::didLoad::%@",result);

}


@end
