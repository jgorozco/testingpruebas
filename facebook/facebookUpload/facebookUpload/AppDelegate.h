//
//  AppDelegate.h
//  facebookupload
//
//  Created by jose garcia on 24/04/12.
//  Copyright (c) 2012 freelance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,FBSessionDelegate>
{
Facebook *facebook;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) Facebook *facebook;
@property (strong, nonatomic) ViewController *viewController;

@end
