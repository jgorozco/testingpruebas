//
//  ViewController.h
//  threads
//
//  Created by Kike on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "protocoloCalculo.h"
#import "Calculos.h"

@interface ViewController : UIViewController <protocoloCalculo>
{
    Calculos *calcuadora;
}
//@property (strong, nonatomic) id <calculoProtocol> delegate;
@property (strong, nonatomic) IBOutlet UIButton *masuno;
@property (strong, nonatomic) IBOutlet UIButton *mastres;

@property (strong, nonatomic) IBOutlet UITextView *cajaTexto;
@property (strong, nonatomic) IBOutlet UITextView *cajaResultado;
@property (strong, nonatomic) IBOutlet UIProgressView *barra;

@end
