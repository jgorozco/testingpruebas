//
//  Calculos.h
//  threads
//
//  Created by Kike on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "protocoloCalculo.h"

@interface Calculos : NSObject
{
    NSObject *protocoloPadre;
}
//@property (nonatomic, weak) id <calculoProtocol> delegate;

-(id) initWithProtocol:(id <protocoloCalculo>) protocolo;
-(void) realizarCalculos;

@end
