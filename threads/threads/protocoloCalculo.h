//
//  protocoloCalculo.h
//  threads
//
//  Created by Kike on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol protocoloCalculo <NSObject>

-(void) iniciar;
-(void) process:(NSNumber *) porcentaje;
-(void) finalizar;

@end
