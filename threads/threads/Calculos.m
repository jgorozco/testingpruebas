//
//  Calculos.m
//  threads
//
//  Created by Kike on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Calculos.h"

@implementation Calculos

-(id) initWithProtocol:(id <protocoloCalculo>) protocolo
{
    self = [super init];
    if(self) {
        //INICIALIZAMOS
        protocoloPadre = protocolo;
    }
    return self;
}


-(void)realizarCalculos 
{
    [protocoloPadre performSelectorOnMainThread:@selector(iniciar) withObject:nil waitUntilDone:YES];
    int i = 0;
    [NSThread sleepForTimeInterval:1.0];
    while (i < 20) {
        i = i + 1;
        float numero=(float)i/20.0;
        NSNumber *num = [NSNumber numberWithFloat:numero];
        [protocoloPadre performSelectorOnMainThread:@selector(process:) withObject:num waitUntilDone:YES];
        [NSThread sleepForTimeInterval:1.0];
    }
    [protocoloPadre performSelectorOnMainThread:@selector(finalizar) withObject:nil waitUntilDone:YES];
}

@end