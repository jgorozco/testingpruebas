//
//  ViewController.m
//  threads
//
//  Created by Kike on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "Calculos.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize masuno, mastres, cajaTexto, cajaResultado, barra;

-(void)iniciar 
{
    NSLog(@"INICIAR del protocolo");
}

-(void)process:(NSNumber *) porcentaje;
{   
    NSLog(@"Porcentaje: %f",[porcentaje floatValue]);
    [self.barra setProgress:[porcentaje floatValue]];
    NSLog(@"PROCESS del protocolo");
}

-(void)finalizar
{
    NSLog(@"FINALIZAR del protocolo");
    self.cajaResultado.text = @"Protocolo finalizado";
}

-(void)actualizarBarra: (float) i
{
    [self.barra setProgress:i];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"viewController::: viewDidLoad");
    self.cajaTexto.text = @"";
    self.cajaResultado.text = @"0";
    self.barra.progress = 0;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    NSLog(@"viewController::: viewDidUnload");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"ViewController::: viewDidAppear");
    Calculos* calculo = [[Calculos alloc] initWithProtocol:self];
    [NSThread detachNewThreadSelector:@selector(realizarCalculos) toTarget:calculo withObject:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ViewController::: ViewDidDisappear");
    [super viewDidDisappear:animated];
}

- (IBAction)sumaruno:(id)sender
{
    NSString *append = [self.cajaTexto.text stringByAppendingString:@"Sumamos 1  -  "];
    self.cajaTexto.text = append;
    self.cajaResultado.text = @"Lo que sea + 1 = Lo que sea";
}

- (IBAction)sumartres:(id)sender
{   
    NSString *append = [self.cajaTexto.text stringByAppendingString:@"Sumamos 3  -  "];
    self.cajaTexto.text = append;
    self.cajaResultado.text = @"Lo que sea + 3 = Lo que sea";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
